FAR : Fulvio Ali Reza
=====================

 A common repository to keep our stuffs. 

 Let's make it closer!


Contributers: 
=============

 - Reza (Akef) Ahmadzadeh
 - Fulvio mastrogiovanni
 - Ali Paikan

Quick guide on git
==================

 To clone the git for the first time: 
    - $ cd ~
    - $ git checkout git@bitbucket.org:apaikan/far.git FAR
    or
    - $ git clone git@bitbucket.org:apaikan/far.git FAR
    
 To update your local repository: 
    - $ cd ~/FAR
    - $ git pull --rebase
 
 To commit and push your own changes: 
    - $ git add <my modified files or folders>
    - $ git commit -m '<the message>'
    - $ git push