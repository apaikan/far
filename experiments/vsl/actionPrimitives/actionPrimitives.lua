#!/usr/bin/lua 

-- Copyright: (C) 2011 Robotics, Brain and Cognitive Sciences - Italian Institute of Technology (IIT)
-- Author: Ali Paikan <ali.paikan@iit.it>
-- Copy Policy: Released under the terms of the LGPLv2.1 or later, see LGPL.TXT


-- LUA_CPATH should have the path to yarp-lua binding library (i.e. yarp.so, yarp.dll) 
require("yarp")

-- Constants ---

shouldExit = false

RIGHT_HAND_HOME = {-0.077916, 0.412204, 0.296346, -0.426454, -0.890622, 0.157892, 2.273602}
HEAD_HOME = {-0.30, 0.10, 0.0}

RIGHT_HAND_ORIENTATION = {0.127097, 0.990996, 0.042116, 3.020013}

OBJECT_PULL_POSES = {orange = {-0.36, 0.26, -0.09}, 
                     green = {-0.36, 0.16, -0.09},
                     yellow = {-0.36, 0.06, -0.09}}


OBJECT_PUSH_POSES = {orange = {-0.10, 0.26, -0.09},
                     green =  {-0.12, 0.16, -0.09},
                     yellow =  {-0.14, 0.06, -0.09}}


-- push min: -0.102719 0.264997 -0.09 0.125876 0.990989 0.045775 3.052931

-------------------------------

yarp.Network()


-- create and open command port
cmdPort = yarp.Port()
cmdPort:open("/actionPrimitives/rpc:i")

--handPort = yarp.port()
--/icub/right_arm/rpc:i
--handPort:

rightArmPort = yarp.Port()
rightArmPort:open("...")
-- connect to the port /armCtrl/right_arm/xd:i 
if not yarp.NetworkBase_connect(rightArmPort:getName(), "/armCtrl/right_arm/xd:i") then
    print("Cannot connect to the righ arm controller!")
    os.exit()
end 


headPort = yarp.Port()
headPort:open("...")
-- connect to the port /iKinGazeCtrl/xd:i
if not yarp.NetworkBase_connect(headPort:getName(), "/iKinGazeCtrl/xd:i") then
    print("Cannot connect to the head controller!")
    os.exit()
end 


torsoPort = yarp.Port()
torsoPort:open("...")
-- connect to the port /iKinGazeCtrl/xd:i
if not yarp.NetworkBase_connect(torsoPort:getName(), "/icub/torso/rpc:i") then
    print("Cannot connect to the torso controller!")
    os.exit()
end 


function respond(port, msg)
    local rep =  yarp.Bottle()
    rep:clear()
    rep:addString(msg)
    port:reply(rep)
end

function go_home()
    local pos = yarp.Bottle()
    local rep =  yarp.Bottle()

    -- arm
    pos:clear();
    for i = 1, #RIGHT_HAND_HOME do
        pos:addDouble(RIGHT_HAND_HOME[i])
    end
    rightArmPort:write(pos)

    yarp.Time_delay(6.0)

    pos:clear()
    pos:addString("set")
    pos:addString("pos")
    pos:addInt(2)
    pos:addDouble(0)
    torsoPort:write(pos, rep);

    pos:clear()
    pos:addString("set")
    pos:addString("pos")
    pos:addInt(0)
    pos:addDouble(8)
    torsoPort:write(pos, rep);

    -- head
    pos:clear();
    for i = 1, #HEAD_HOME do
        pos:addDouble(HEAD_HOME[i])
    end
    headPort:write(pos);

end

repeat
    local cmd = yarp.Bottle()
    cmd:clear()
    local ret = cmdPort:read(cmd, true)
    if ret == true and cmd:size() > 0 then 
        print('Received:', cmd:toString())

        -- exit command
        if cmd:toString() == "exit" then
            shouldExit = true
            respond(cmdPort, "[ok]")

        -- home
        elseif cmd:toString() == "home" then
            go_home()
            respond(cmdPort, "[ok]")            
        -- pull object    
        elseif cmd:get(0):asString() == "pull" then
            if cmd:size() < 2 then
                respond(cmdPort, "[error]")
            else
                obj_pos = OBJECT_PULL_POSES[cmd:get(1):asString()];
                if obj_pos == nil then
                    respond(cmdPort, "[error]")                    
                else                    
                    pos = yarp.Bottle()

                    -- move far
                    pos:clear();
                    for i = 1, #obj_pos do
                        pos:addDouble(obj_pos[i])
                    end
                    headPort:write(pos)

                    for i = 1, #RIGHT_HAND_ORIENTATION do
                        pos:addDouble(RIGHT_HAND_ORIENTATION[i])
                    end
                    print(pos:toString())
                    rightArmPort:write(pos)                    
                    -- wait for action is done
                    yarp.Time_delay(4.0)
                
                    
                    -- move close
                    pos:clear();
                    pos:addDouble(obj_pos[1] + 0.14)
                    for i = 2, #obj_pos do
                        pos:addDouble(obj_pos[i])
                    end
                    headPort:write(pos)
                    for i = 1, #RIGHT_HAND_ORIENTATION do
                        pos:addDouble(RIGHT_HAND_ORIENTATION[i])
                    end
                    print(pos:toString())
                    rightArmPort:write(pos)
                    -- wait for action is done
                    yarp.Time_delay(2.0)
                    --[[
                    go_home()
                    --]]
                    respond(cmdPort, "[ok]")
                end                    
            end
        -- push object    
        elseif cmd:get(0):asString() == "push" then
            if cmd:size() < 2 then
                respond(cmdPort, "[error]")
            else
                obj_pos = OBJECT_PUSH_POSES[cmd:get(1):asString()];
                if obj_pos == nil then
                    respond(cmdPort, "[error]")                    
                else                    
                    pos = yarp.Bottle()
                    -- move top
                    pos:clear();
                    for i = 1, #obj_pos-1 do
                        pos:addDouble(obj_pos[i])
                    end
                    pos:addDouble(obj_pos[3] + 0.1)
                    
                    for i = 1, #RIGHT_HAND_ORIENTATION do
                        pos:addDouble(RIGHT_HAND_ORIENTATION[i])
                    end
                    print(pos:toString())
                    rightArmPort:write(pos)
                    -- wait for action is done
                    yarp.Time_delay(3.5)

                    -- move down
                    pos:clear();
                    for i = 1, #obj_pos do
                        pos:addDouble(obj_pos[i])
                    end
                    headPort:write(pos)
                    for i = 1, #RIGHT_HAND_ORIENTATION do
                        pos:addDouble(RIGHT_HAND_ORIENTATION[i])
                    end
                    print(pos:toString())
                    rightArmPort:write(pos)                    
                    -- wait for action is done
                    yarp.Time_delay(2.0)

                    -- move close
                    pos:clear();
                    pos:addDouble(obj_pos[1] - 0.15)
                    for i = 2, #obj_pos do
                        pos:addDouble(obj_pos[i])
                    end
                    headPort:write(pos)
                    for i = 1, #RIGHT_HAND_ORIENTATION do
                        pos:addDouble(RIGHT_HAND_ORIENTATION[i])
                    end
                    print(pos:toString())
                    rightArmPort:write(pos)
                    -- wait for action is done
                    yarp.Time_delay(3.5)
                    --[[

                    -- move top
                    pos:clear();
                    pos:addDouble(obj_pos[1] - 0.15)
                    pos:addDouble(obj_pos[2])
                    pos:addDouble(obj_pos[3] + 0.05)
                    for i = 1, #RIGHT_HAND_ORIENTATION do
                        pos:addDouble(RIGHT_HAND_ORIENTATION[i])
                    end
                    print(pos:toString())
                    rightArmPort:write(pos)
                    -- wait for action is done
                    yarp.Time_delay(2.0)
                    go_home()
                    --]]
                    respond(cmdPort, "[ok]")
                end                    
            end

        else 
            respond(cmdPort, "[error]")
        end
    end
until shouldExit ~= false


cmdPort:close()
rightArmPort:close()
headPort:close()
torsoPort:close()

--yarp.Network_fini()

