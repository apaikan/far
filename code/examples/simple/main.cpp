// -*- mode:C++; tab-width:4; c-basic-offset:4; indent-tabs-mode:nil -*-
/* 
 * Copyright (C) 2012 Department of Robotics Brain and Cognitive Sciences - Istituto Italiano di Tecnologia
 * Author: Ali Paikan
 * email:  ali.paikan@iit.it
 * Permission is granted to copy, distribute, and/or modify this program
 * under the terms of the GNU General Public License, version 2 or any
 * later version published by the Free Software Foundation.
 *
 * A copy of the license can be found at
 * http://www.robotcub.org/icub/license/gpl.txt
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details
*/
#include <stdio.h>
#include <yarp/os/Network.h>

#include <yarp/os/RFModule.h>
using namespace yarp::os;


class MyModule: public RFModule
{
public:

    MyModule(){
    }

    ~MyModule() {

    }


    bool configure(ResourceFinder &rf)
    {               
        if(rf.check("period"))
            period = rf.find("period").asDouble(); 
        else
            period = 0.5;

        return true;
    }

    double getPeriod() {
        return period;
    }
    
    bool updateModule()
    { 
        // main code goes here
        printf("Hello Yarp\n");
        return true; 
    }

    bool interruptModule()
    {
        fprintf(stderr, "Interrupting\n");
        return true;
    }

    bool close()
    {
        fprintf(stderr, "Calling close\n");
        return true;
    }

private:
   double period; 

};


int main(int argc, char *argv[]) 
{
    Network yarp;

    MyModule module;
    ResourceFinder rf;
    //rf.setVerbose();
	//rf.setDefaultConfigFile("face_detector.ini");
    //rf.setDefaultContext("faceDetector/conf");
    rf.configure("ROOT_ROOT", argc, argv);
   
    if (!module.configure(rf))
    {
        fprintf(stderr, "Error configuring module returning\n");
        return -1;
    }
    
    module.runModule();

    printf("Module shutting down\n");

    return 0;
}

