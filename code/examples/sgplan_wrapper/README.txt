
A wrapper class to call the sgplan planner from C++ using ramdisk
=================================================================

The Wrapper communicates with the sgplan binary file through tmpfs (ramdisk) file 
system by creating the tmpfs and mounting it in the /tmp folder. Therefore, a sudo 
user permission is required. 

Alternatively, the tmpfs ramdisk can be created in advance to the execution of the 
program. In this way, the planner can be initialized to not create the tmpfs (i.e., 
SGWrapper::init(false,...)) and sudo user permission is not required anymore. 


Dependencies
============
1) A UNIX-based distribution (e.g, Linux) 
2) CMake to compile the example
3) sgplan522 binary file


Quick guide to build the example: 
==================================

1) $ export PATH=$PATH:<path to the sgplan522>

2) $ cd sgplan_wrapper
   $ mkdir build
   $ cd build
   $ cmake ../
   $ make

3) $ sudo ./sgplan_wrapper


Author
======
 Ali Paikan (ali.paikan@iit.it)

