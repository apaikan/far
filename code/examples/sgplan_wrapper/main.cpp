/*
 *  Copy Policy: Released under the terms of the LGPLv2.1 or later, see LGPL.TXT
 *  Authors: Ali Paikan <ali.paikan@iit.it>
 *
 */

#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/mman.h>
#include <iostream>

#include "sgwrapper.h"

#define PROBLEM_MAP "\
 --- ---       --- --- \n\
| A | B |     | C | A |\n\
 --- ---   =>  --- --- \n\
| C |   |     | B |   |\n\
 --- ---       --- --- "

#define BOXES_DOMAIN "\
(define (domain robot)\
(:predicates (area ?a) (box ?b) (hand ?h)\
		     (at ?b ?a) (free ?g) (empty ?a) (carry ?o ?g))\
\
(:action pick\
 :parameters (?b ?a ?h)\
 :precondition (and (box ?b)  (area ?a)  (hand ?h)\
                    (at ?b ?a) (not (empty ?a)) (free ?h))\
 :effect (and (carry ?b ?h) (empty ?a) (not (at ?b ?a))	(not (free ?h))))\
\
(:action put\
 :parameters (?b ?a ?h)\
 :precondition (and (box ?b) (area ?a) (hand ?h) (empty ?a) (carry ?b ?h))\
 :effect (and (at ?b ?a) (free ?h) (not (empty ?a)) (not (carry ?b ?h)))))"


#define BOXES_PROBLEM "\
(define (problem chessboard-boxes)\
 (:domain robot)\
 (:objects P11 P12 P21 P22 A B C hand)\
 (:init (area P11) (area P12) (area P21) (area P22)\
        (box A) (box B) (box C)\
        (hand hand) (free hand)\
        (at A P11) (at B P12) (at C P21) (empty P22))\
 (:goal (and (at C P11) (at A P12) (at B P21))))"


using namespace std; 

int main(int argc, char* argv[])
{
    SGWrapper sgw;

    // initializing the plan wrapper
    if(!sgw.init(true))
    {
        printf("%s\n", sgw.getStrError());
        return 0;
    }    

    // setting the pddl domain and problem 
    sgw.updateDomain(BOXES_DOMAIN);
    sgw.updateProblem(BOXES_PROBLEM);

    // planning 
    string res;
    if(!sgw.plan(res))
    {
        printf("%s\n", sgw.getStrError());
        return 0;
    }
    
    // parsing the results 
    SGWrapper::ActionList actions;
    sgw.pars(res.c_str(), actions);

    // printing the planner result
    printf("Problem  :\n%s\n\n", PROBLEM_MAP);
    printf("Solution:\n");
    for(int i=0; i<actions.size(); i++)
        printf("action [%d] : %s\n", i, actions[i].c_str());
    return 0;
}

