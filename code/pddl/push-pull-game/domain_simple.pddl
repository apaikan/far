(define (domain icub)

(:types box)

(:predicates (far ?b - box))

;;
; Pulling action
;;
(:action pull
 :parameters (?b - box)
 :precondition (far ?b)
 :effect (not (far ?b)))

;;
; Pushing action
;;
(:action push
 :parameters (?b - box)
 :precondition (not (far ?b))
 :effect (far ?b))

)

