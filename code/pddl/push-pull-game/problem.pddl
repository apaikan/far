(define (problem push_pull-game)
  (:domain icub)
  
  (:objects RED GREEN BLUE)
  
  (:init (box RED) (box GREEN)  (box BLUE)
		 (far RED) (near GREEN) (far BLUE))
              
  (:goal (and (near RED) (far GREEN) (near BLUE)))
)

