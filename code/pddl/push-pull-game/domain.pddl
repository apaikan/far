(define (domain icub)

(:predicates (box  ?b)
			 (far  ?f)
             (near ?n))

;;
; Pulling action
;;
(:action pull
 :parameters (?b)
 :precondition (and (box ?b) (far ?b))
 :effect (and (not (far ?b)) (near ?b)))

;;
; Pushing action
;;
(:action push
 :parameters (?b)
 :precondition (and (box ?b) (near ?b))
 :effect (and (not(near ?b)) (far ?b)))
)

