(define (problem push_pull-game)
  (:domain icub)
  
  (:objects RED GREEN BLUE - box)
  
  (:init (far RED) (not (far GREEN)) (far BLUE))
              
  (:goal (and (not (far RED)) (far GREEN) (not (far BLUE)) ) )
)

