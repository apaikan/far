// -*- mode:C++; tab-width:4; c-basic-offset:4; indent-tabs-mode:nil -*-
/* 
 * Copyright (C) 2012 Department of Robotics Brain and Cognitive Sciences - Istituto Italiano di Tecnologia
 * Author: Ali Paikan
 * email:  ali.paikan@iit.it
 * Permission is granted to copy, distribute, and/or modify this program
 * under the terms of the GNU General Public License, version 2 or any
 * later version published by the Free Software Foundation.
 *
 * A copy of the license can be found at
 * http://www.robotcub.org/icub/license/gpl.txt
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details
*/

#include <stdio.h>
#include <string>
#include <yarp/os/Network.h>
#include <yarp/os/RFModule.h>
#include <yarp/os/RpcServer.h>
#include "pddlPlanner_IDL.h"

using namespace std;
using namespace yarp::os;


#include <sgwrapper.h>

class MyModule: public RFModule, public pddlPlanner_IDL 
{
private:
    RpcServer cmdPort;
    SGWrapper sgw;

public:

    bool configure(ResourceFinder &rf)
    {              
        // initializing the plan wrapper
        if(!sgw.init(true))
        {
            printf("Cannot initialize planner (%s)\n", sgw.getStrError());
            return false;
        }    

        if(!cmdPort.open("/pddlPlanner/rpc:i"))
            return false;
        attach(cmdPort); 
        return true;
    }

    bool attach(yarp::os::RpcServer &source) {
      return this->yarp().attachAsServer(source);
    }

    double getPeriod() {
        return 10.0;
    }
    
    bool updateModule() { 
        // main code goes here
        printf("pddlPlanner is running happily...\n");
        return true; 
    }

    bool interruptModule() { return true; }

    bool close() {
        fprintf(stderr, "Calling close\n");
        cmdPort.close();
        return true;
    }

    /**
     * Update PDDL domain
     * @param domain specifies the domain string
     * @return true/false on success/failure
     */
    virtual bool updateDomain(const std::string& domain) {
        sgw.updateDomain(domain.c_str());
    }

    /**
     * Update PDDL problem
     * @param problem specifies the problem string
     * @return true/false on success/failure
     */
     virtual bool updateProblem(const std::string& problem) {
        sgw.updateProblem(problem.c_str());
     }

    /**
     * Run the planner and retrieve the results
     * @return a list of actions in string or empty list on failure
     */
     virtual std::vector<std::string>  plan() {
        string res;
        SGWrapper::ActionList actions;
        if(!sgw.plan(res))
        {
            printf("%s\n", sgw.getStrError());
            return actions;
        }

        // parsing the results 
        sgw.pars(res.c_str(), actions);
        return actions;
     }
};


int main(int argc, char *argv[]) 
{
    Network yarp;

    MyModule module;
    ResourceFinder rf;
    //rf.setVerbose();
	//rf.setDefaultConfigFile("face_detector.ini");
    //rf.setDefaultContext("faceDetector/conf");
    rf.configure("ROOT_ROOT", argc, argv);
   
    if (!module.configure(rf))
    {
        fprintf(stderr, "Error configuring module returning\n");
        return -1;
    }
    
    module.runModule();

    printf("Module shutting down\n");

    return 0;
}

