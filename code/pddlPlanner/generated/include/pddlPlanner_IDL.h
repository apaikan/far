// This is an automatically-generated file.
// It could get re-generated if the ALLOW_IDL_GENERATION flag is on.

#ifndef YARP_THRIFT_GENERATOR_pddlPlanner_IDL
#define YARP_THRIFT_GENERATOR_pddlPlanner_IDL

#include <yarp/os/Wire.h>
#include <yarp/os/idl/WireTypes.h>

class pddlPlanner_IDL;


/**
 * pddlPlanner_IDL
 * Interface.
 */
class pddlPlanner_IDL : public yarp::os::Wire {
public:
  pddlPlanner_IDL() { yarp().setOwner(*this); }
/**
 * Update PDDL domain
 * @param domain specifies the domain string
 * @return true/false on success/failure
 */
  virtual bool updateDomain(const std::string& domain);
/**
 * Update PDDL problem
 * @param problem specifies the problem string
 * @return true/false on success/failure
 */
  virtual bool updateProblem(const std::string& problem);
/**
 * Run the planner and retrieve the results
 * @return a list of actions in string or empty list on failure
 */
  virtual std::vector<std::string>  plan();
  virtual bool read(yarp::os::ConnectionReader& connection);
  virtual std::vector<std::string> help(const std::string& functionName="--all");
};

#endif

