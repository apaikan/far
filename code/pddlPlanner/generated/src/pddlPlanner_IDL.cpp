// This is an automatically-generated file.
// It could get re-generated if the ALLOW_IDL_GENERATION flag is on.

#include <pddlPlanner_IDL.h>
#include <yarp/os/idl/WireTypes.h>



class pddlPlanner_IDL_updateDomain : public yarp::os::Portable {
public:
  std::string domain;
  bool _return;
  virtual bool write(yarp::os::ConnectionWriter& connection) {
    yarp::os::idl::WireWriter writer(connection);
    if (!writer.writeListHeader(2)) return false;
    if (!writer.writeTag("updateDomain",1,1)) return false;
    if (!writer.writeString(domain)) return false;
    return true;
  }
  virtual bool read(yarp::os::ConnectionReader& connection) {
    yarp::os::idl::WireReader reader(connection);
    if (!reader.readListReturn()) return false;
    if (!reader.readBool(_return)) {
      reader.fail();
      return false;
    }
    return true;
  }
};

class pddlPlanner_IDL_updateProblem : public yarp::os::Portable {
public:
  std::string problem;
  bool _return;
  virtual bool write(yarp::os::ConnectionWriter& connection) {
    yarp::os::idl::WireWriter writer(connection);
    if (!writer.writeListHeader(2)) return false;
    if (!writer.writeTag("updateProblem",1,1)) return false;
    if (!writer.writeString(problem)) return false;
    return true;
  }
  virtual bool read(yarp::os::ConnectionReader& connection) {
    yarp::os::idl::WireReader reader(connection);
    if (!reader.readListReturn()) return false;
    if (!reader.readBool(_return)) {
      reader.fail();
      return false;
    }
    return true;
  }
};

class pddlPlanner_IDL_plan : public yarp::os::Portable {
public:
  std::vector<std::string>  _return;
  virtual bool write(yarp::os::ConnectionWriter& connection) {
    yarp::os::idl::WireWriter writer(connection);
    if (!writer.writeListHeader(1)) return false;
    if (!writer.writeTag("plan",1,1)) return false;
    return true;
  }
  virtual bool read(yarp::os::ConnectionReader& connection) {
    yarp::os::idl::WireReader reader(connection);
    if (!reader.readListReturn()) return false;
    {
      _return.clear();
      uint32_t _size0;
      yarp::os::idl::WireState _etype3;
      reader.readListBegin(_etype3, _size0);
      _return.resize(_size0);
      uint32_t _i4;
      for (_i4 = 0; _i4 < _size0; ++_i4)
      {
        if (!reader.readString(_return[_i4])) {
          reader.fail();
          return false;
        }
      }
      reader.readListEnd();
    }
    return true;
  }
};

bool pddlPlanner_IDL::updateDomain(const std::string& domain) {
  bool _return = false;
  pddlPlanner_IDL_updateDomain helper;
  helper.domain = domain;
  if (!yarp().canWrite()) {
    fprintf(stderr,"Missing server method '%s'?\n","bool pddlPlanner_IDL::updateDomain(const std::string& domain)");
  }
  bool ok = yarp().write(helper,helper);
  return ok?helper._return:_return;
}
bool pddlPlanner_IDL::updateProblem(const std::string& problem) {
  bool _return = false;
  pddlPlanner_IDL_updateProblem helper;
  helper.problem = problem;
  if (!yarp().canWrite()) {
    fprintf(stderr,"Missing server method '%s'?\n","bool pddlPlanner_IDL::updateProblem(const std::string& problem)");
  }
  bool ok = yarp().write(helper,helper);
  return ok?helper._return:_return;
}
std::vector<std::string>  pddlPlanner_IDL::plan() {
  std::vector<std::string>  _return;
  pddlPlanner_IDL_plan helper;
  if (!yarp().canWrite()) {
    fprintf(stderr,"Missing server method '%s'?\n","std::vector<std::string>  pddlPlanner_IDL::plan()");
  }
  bool ok = yarp().write(helper,helper);
  return ok?helper._return:_return;
}

bool pddlPlanner_IDL::read(yarp::os::ConnectionReader& connection) {
  yarp::os::idl::WireReader reader(connection);
  reader.expectAccept();
  if (!reader.readListHeader()) { reader.fail(); return false; }
  yarp::os::ConstString tag = reader.readTag();
  while (!reader.isError()) {
    // TODO: use quick lookup, this is just a test
    if (tag == "updateDomain") {
      std::string domain;
      if (!reader.readString(domain)) {
        reader.fail();
        return false;
      }
      bool _return;
      _return = updateDomain(domain);
      yarp::os::idl::WireWriter writer(reader);
      if (!writer.isNull()) {
        if (!writer.writeListHeader(1)) return false;
        if (!writer.writeBool(_return)) return false;
      }
      reader.accept();
      return true;
    }
    if (tag == "updateProblem") {
      std::string problem;
      if (!reader.readString(problem)) {
        reader.fail();
        return false;
      }
      bool _return;
      _return = updateProblem(problem);
      yarp::os::idl::WireWriter writer(reader);
      if (!writer.isNull()) {
        if (!writer.writeListHeader(1)) return false;
        if (!writer.writeBool(_return)) return false;
      }
      reader.accept();
      return true;
    }
    if (tag == "plan") {
      std::vector<std::string>  _return;
      _return = plan();
      yarp::os::idl::WireWriter writer(reader);
      if (!writer.isNull()) {
        if (!writer.writeListHeader(1)) return false;
        {
          if (!writer.writeListBegin(BOTTLE_TAG_STRING, static_cast<uint32_t>(_return.size()))) return false;
          std::vector<std::string> ::iterator _iter5;
          for (_iter5 = _return.begin(); _iter5 != _return.end(); ++_iter5)
          {
            if (!writer.writeString((*_iter5))) return false;
          }
          if (!writer.writeListEnd()) return false;
        }
      }
      reader.accept();
      return true;
    }
    if (tag == "help") {
      std::string functionName;
      if (!reader.readString(functionName)) {
        functionName = "--all";
      }
      std::vector<std::string> _return=help(functionName);
      yarp::os::idl::WireWriter writer(reader);
        if (!writer.isNull()) {
          if (!writer.writeListHeader(2)) return false;
          if (!writer.writeTag("many",1, 0)) return false;
          if (!writer.writeListBegin(BOTTLE_TAG_INT, static_cast<uint32_t>(_return.size()))) return false;
          std::vector<std::string> ::iterator _iterHelp;
          for (_iterHelp = _return.begin(); _iterHelp != _return.end(); ++_iterHelp)
          {
            if (!writer.writeString(*_iterHelp)) return false;
           }
          if (!writer.writeListEnd()) return false;
        }
      reader.accept();
      return true;
    }
    if (reader.noMore()) { reader.fail(); return false; }
    yarp::os::ConstString next_tag = reader.readTag();
    if (next_tag=="") break;
    tag = tag + "_" + next_tag;
  }
  return false;
}

std::vector<std::string> pddlPlanner_IDL::help(const std::string& functionName) {
  bool showAll=(functionName=="--all");
  std::vector<std::string> helpString;
  if(showAll) {
    helpString.push_back("*** Available commands:");
    helpString.push_back("updateDomain");
    helpString.push_back("updateProblem");
    helpString.push_back("plan");
    helpString.push_back("help");
  }
  else {
    if (functionName=="updateDomain") {
      helpString.push_back("bool updateDomain(const std::string& domain) ");
      helpString.push_back("Update PDDL domain ");
      helpString.push_back("@param domain specifies the domain string ");
      helpString.push_back("@return true/false on success/failure ");
    }
    if (functionName=="updateProblem") {
      helpString.push_back("bool updateProblem(const std::string& problem) ");
      helpString.push_back("Update PDDL problem ");
      helpString.push_back("@param problem specifies the problem string ");
      helpString.push_back("@return true/false on success/failure ");
    }
    if (functionName=="plan") {
      helpString.push_back("std::vector<std::string>  plan() ");
      helpString.push_back("Run the planner and retrieve the results ");
      helpString.push_back("@return a list of actions in string or empty list on failure ");
    }
    if (functionName=="help") {
      helpString.push_back("std::vector<std::string> help(const std::string& functionName=\"--all\")");
      helpString.push_back("Return list of available commands, or help message for a specific function");
      helpString.push_back("@param functionName name of command for which to get a detailed description. If none or '--all' is provided, print list of available commands");
      helpString.push_back("@return list of strings (one string per line)");
    }
  }
  if ( helpString.empty()) helpString.push_back("Command not found");
  return helpString;
}


