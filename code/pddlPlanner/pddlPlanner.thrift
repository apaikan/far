# Copyright: (C) 2013 iCub Facility - Istituto Italiano di Tecnologia
# Authors: Ali Paikan
# CopyPolicy: Released under the terms of the GNU GPL v2.0.
#
# pddlPlanner.thrift

/**
* pddlPlanner_IDL
*
* Interface. 
*/

service pddlPlanner_IDL
{
 
  /**
  * Update PDDL domain 
  * @param domain specifies the domain string
  * @return true/false on success/failure
  */
  bool updateDomain(1:string domain);
  
  /**
  * Update PDDL problem 
  * @param problem specifies the problem string
  * @return true/false on success/failure
  */
  bool updateProblem(1:string problem);

  /**
  * Run the planner and retrieve the results 
  * @return a list of actions in string or empty list on failure 
  */
  list<string> plan();

}
