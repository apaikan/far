#!/usr/bin/lua 

-- Copyright: (C) 2011 Robotics, Brain and Cognitive Sciences - Italian Institute of Technology (IIT)
-- Author: Ali Paikan <ali.paikan@iit.it>
-- Copy Policy: Released under the terms of the LGPLv2.1 or later, see LGPL.TXT


-- LUA_CPATH should have the path to yarp-lua binding library (i.e. yarp.so, yarp.dll) 
require("yarp")

yarp.Network()


-- create and open command port
local cmd = yarp.Bottle();
local reply =  yarp.Bottle()
cmdPort = yarp.Port()
cmdPort:open("/test_planner/rpc:o")
-- connect to planner port
if not yarp.NetworkBase_connect(cmdPort:getName(), "/pddlPlanner/rpc:i") then
    print("Cannot connect to the pddlPlanner port!")
    os.exit()
end 

local problem_graph = "\
 --- ---       --- --- \
| A | B |     | C | A |\
 --- ---   =>  --- --- \
| C |   |     | B |   |\
 --- ---       --- --- \
"
 
 
local boxes_domain = "\
(define (domain robot)\
(:predicates (area ?a) (box ?b) (hand ?h)\
		     (at ?b ?a) (free ?g) (empty ?a) (carry ?o ?g))\
\
(:action pick\
 :parameters (?b ?a ?h)\
 :precondition (and (box ?b)  (area ?a)  (hand ?h)\
                    (at ?b ?a) (not (empty ?a)) (free ?h))\
 :effect (and (carry ?b ?h) (empty ?a) (not (at ?b ?a))	(not (free ?h))))\
\
(:action put\
 :parameters (?b ?a ?h)\
 :precondition (and (box ?b) (area ?a) (hand ?h) (empty ?a) (carry ?b ?h))\
 :effect (and (at ?b ?a) (free ?h) (not (empty ?a)) (not (carry ?b ?h)))))"

local boxes_problem = "\
(define (problem chessboard-boxes)\
 (:domain robot)\
 (:objects P11 P12 P21 P22 A B C hand)\
 (:init (area P11) (area P12) (area P21) (area P22)\
        (box A) (box B) (box C)\
        (hand hand) (free hand)\
        (at A P11) (at B P12) (at C P21) (empty P22))\
 (:goal (and (at C P11) (at A P12) (at B P21))))"


-- update the domain 
cmd:clear()
cmd:addString("updateDomain")
cmd:addString(boxes_domain)
cmdPort:write(cmd, reply)
print(reply:toString())

-- update the problem
cmd:clear()
cmd:addString("updateProblem")
cmd:addString(boxes_problem)
cmdPort:write(cmd, reply)
print(reply:toString())

print(problem_graph)

-- plan and get the actions
cmd:clear()
cmd:addString("plan")
cmdPort:write(cmd, reply)
if reply:size() > 0 then
    actions = reply:get(0):asList()
    for i=0, actions:size()-1 do 
        print(actions:get(i):asString())
    end
else
    print("Failed to plan!")
end

cmdPort:close()

--yarp.Network_fini()

