/*
 *  Copy Policy: Released under the terms of the LGPLv2.1 or later, see LGPL.TXT
 *  Authors: Ali Paikan <ali.paikan@iit.it>
 *
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <assert.h>
#include <stdarg.h>
#include <strings.h>

#include "sgwrapper.h"


#include <sstream>
using namespace std;

#ifdef WITH_DEBUG 
#define PrintError(format, ...) {\
    snprintf(szError, ERRMESAGE_SIZE-1, format, __VA_ARGS__ );\
    fprintf(stderr, "[SGWrapper] " format "\n", __VA_ARGS__ ); }
#else
#define PrintError(format, ...)\
    snprintf(szError, ERRMESAGE_SIZE-1, format, __VA_ARGS__ )
#endif


/**
 * Simple Constructor and Destructor
 */
SGWrapper::SGWrapper()
{
    shouldUmount = false;
    bAllOk = false;
    bzero(szError, ERRMESAGE_SIZE);    
}

SGWrapper::~SGWrapper()
{
    if(shouldUmount)
        umount(strSgpTmpfs.c_str());
}

/**
 * Initialize the planner
 */
bool SGWrapper::init(bool create_tmpfs, 
                     const char* sgp_cmd, const char* sgp_tmpfs)
{
    if(bAllOk)
        return true;

    assert(sgp_tmpfs != NULL); 
    assert(sgp_cmd != NULL);

    strSgpTmpfs = sgp_tmpfs;
    strSgpCmd = sgp_cmd;
    
    if(create_tmpfs) 
    {   
        const char* src  = "SGWrapper";
        const char* type = "tmpfs";
        const unsigned long mntflags = 0;
        const char* opts = "mode=0700,uid=65534";   /* 65534 is the uid of nobody */
        int ret; 

        ret = mkdir(sgp_tmpfs, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        if((ret != 0) && (errno != EEXIST))
        {
            PrintError("Error : Failed to create %s! (%s [%d])", sgp_tmpfs, strerror(errno), errno); 
            return false;
        }

        if((ret = mount(src, sgp_tmpfs, type, mntflags, opts)) != 0)
        {
            PrintError("Error : Failed to mount %s! (%s [%d]) ", sgp_tmpfs, strerror(errno), errno); 
            return false;
        }
        shouldUmount = true;        
    }

    bAllOk = true;
    return bAllOk;
}

/**
 * update pddl domain (operators) file
 */
bool SGWrapper::updateDomain(const char* domain)
{
    if(!bAllOk)
        return false;

    assert(domain != NULL);
    strDomain = domain;
    string filename = strSgpTmpfs + "/domain.pddl";
    FILE* fdomain = fopen(filename.c_str(), "w+");
    if(fdomain == NULL)
    {
       PrintError( "Error : Failed to update domain file %s! (%s [%d])", 
                filename.c_str(), strerror(errno), errno); 
        return false;       
    }
    fputs(domain, fdomain);
    fclose(fdomain);
    return true;
}

/**
 * update pddl problem (facts) file
 */
bool SGWrapper::updateProblem(const char* problem)
{
    if(!bAllOk)
        return false;

    assert(problem != NULL);

    strProblem = problem;
    assert(problem != NULL);
    strDomain = problem;
    string filename = strSgpTmpfs + "/problem.pddl";
    FILE* fproblem = fopen(filename.c_str(), "w+");
    if(fproblem == NULL)
    {
       PrintError( "Error : Failed to update problem file %s! (%s [%d])", 
                filename.c_str(), strerror(errno), errno); 
        return false;       
    }
    fputs(problem, fproblem);
    fclose(fproblem);
    return true;
}

/**
 * run the planner and return the result
 */
bool SGWrapper::plan(std::string &result)
{ 
    char buffer[256];
    ostringstream cmd;
    cmd<<strSgpCmd<<" -o "<<strSgpTmpfs<<"/domain.pddl -f "
       <<strSgpTmpfs<<"/problem.pddl -out "<<strSgpTmpfs<<"/result.out 2>&1";

    FILE* fpipe = popen(cmd.str().c_str(), "r");
    if(!fpipe) 
    {
        PrintError( "Error : Failed to run %s! (%s [%d])", 
                cmd.str().c_str(), strerror(errno), errno); 
        return false;
    }       
    
    string strTmp = "";
    while(!feof(fpipe)) 
    {
    	if(fgets(buffer, 128, fpipe) != NULL)
            if(buffer[0] != '#')
    		    strTmp += buffer;
    }
    pclose(fpipe);
    PrintError("%s", strTmp.c_str());

    string filename = strSgpTmpfs + "/result.out";
    FILE* fresult = fopen(filename.c_str(), "r");
    if(fresult == NULL)
    {
        //PrintError( "Error : Failed to access result file %s! (%s [%d])", 
        //        filename.c_str(), strerror(errno), errno); 
        return false;       
    }    
    result = "";
    while(!feof(fresult)) 
    {
    	if(fgets(buffer, 128, fresult) != NULL)
    		    result += buffer;
    }
    fclose(fresult);
    return true;
}

/**
 * pars the output of planner into sepearte action strings
 */
bool SGWrapper::pars(const char* plan_result, SGWrapper::ActionList& actions)
{
    actions.clear();
    stringstream ss(plan_result);
    string line;
    while(getline(ss, line))
    {
        if((line != "") && (line[0] != ';'))
        {
            size_t pos1 = line.find('(');   
            size_t pos2 = line.find(')');
            if((pos1 != string::npos) && (pos2 != string::npos))
                actions.push_back(line.substr(pos1+1, pos2-pos1-1));
            else
            {
                PrintError("Error in parsing %s", line.c_str());
                return false;
            }
        }
    }
    return true;
}

