/*
 *  Copy Policy: Released under the terms of the LGPLv2.1 or later, see LGPL.TXT
 *  Authors: Ali Paikan <ali.paikan@iit.it>
 *
 */

#ifndef _SGWRAPPER_H_
#define _SGWRAPPER_H_

#define SGPLAN_CMD      "sgplan522"
#define SGPLAN_TMPFS    "/tmp/sgplan"
#define ERRMESAGE_SIZE  256

#include <string>
#include <vector>

class SGWrapper {

public:
    /**
     * A container of action strings 
     */
    typedef std::vector<std::string> ActionList;
    typedef std::vector<std::string>::iterator ActionIter;

public:
    
    /**
     * Simple Constructor and Destructor
     */
    SGWrapper();

    ~SGWrapper();

    /**
     * Initialize the planner
     */
    bool init(bool create_tmpfs = false,
              const char* sgp_cmd = SGPLAN_CMD, 
              const char* sgp_tmpfs = SGPLAN_TMPFS);
              

    /**
     * update pddl domain (operators) file
     */
    bool updateDomain(const char* domain);

    /**
     * update pddl problem (facts) file
     */
    bool updateProblem(const char* problem);

    /**
     * run the planner and return the result
     */
    bool plan(std::string &result);

    /**
     * pars the output of planner into sepearte action strings
     */
    bool pars(const char* plan_result, SGWrapper::ActionList& actions);

    /*
     * get the last error message
     */
    const char* getStrError(void) {
        return szError;
    }

private:
    bool bAllOk;
    bool shouldUmount;
    std::string strSgpCmd;
    std::string strSgpTmpfs;
    std::string strDomain;
    std::string strProblem;
    char szError[ERRMESAGE_SIZE];
};

#endif //_SGWRAPPER_H_


